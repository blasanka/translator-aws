import 'package:flutter/material.dart';
import 'package:translator/translator.dart';

void main () async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build (BuildContext context) {
    return MaterialApp(
      title: 'Flutter GTranslate Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter GTrans Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage ({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState () => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final String textToTranslate = "I would buy a car, if I had money.";
  String toLang = 'en';

  Future translateText ({String textToTranslate = "", String toLangCode = 'en'}) async {
    GoogleTranslator translator = GoogleTranslator();

    String input = "Здравствуйте. Ты в порядке?";

    translator.translate(input, to: 'en')
        .then((s) =>
        print("Source: " + input + "\n"
            "Translated: " + s + "\n"));

    translator.baseUrl = "https://translate.google.cn/translate_a/single";
    translator.translateAndPrint("This means 'testing' in chinese", to: 'zh-cn');

    var translation = await translator.translate(
        textToTranslate, from: 'en', to: toLang);
    print("translation: " + translation);
    return translation;
  }

  @override
  Widget build (BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: FutureBuilder(
            future: translateText(textToTranslate: textToTranslate),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                  return Text("No data");
                case ConnectionState.waiting:
                case ConnectionState.active:
                  return CircularProgressIndicator();
                case ConnectionState.done:
                  if (snapshot.hasError) {
                    return Text("err");
                  } else {
                    return Text(snapshot.data);
                  }
              }
            }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            toLang = 'si';
          });
        },
        tooltip: 'Translate',
        child: Icon(Icons.transform),
      ),);
  }
}
